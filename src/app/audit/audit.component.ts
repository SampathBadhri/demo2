import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

import { Audit } from '@/_models';
import { AuditService, AuthenticationService } from '@/_services';

@Component({ templateUrl: 'audit.component.html' })
export class AuditComponent implements OnInit {
    audits = [];
    sortOrder = 'loginTime';
    time = '12-hour-format';
    format = 'dd/MM/yyyy hh:mm:ss';
    constructor(
        private authenticationService: AuthenticationService,
        private auditService: AuditService
    ) {
    }

    ngOnInit() {
        this.loadAllAudits();
    }

    private loadAllAudits() {
        this.auditService.getAll()
            .pipe(first())
            .subscribe(audits => {
                audits.sort((a, b) => (a.loginTime > b.loginTime ? -1 : 1));
                audits.map(e => {
                    let loginTime = new Date(parseInt(e.loginTime));
                    let logoutTime = new Date(parseInt(e.logoutTime));
                    e["convertedloginTime"] = new DatePipe('en-US').transform(loginTime, 'dd/MM/yyyy hh:mm:ss');
                    e["convertedlogoutTime"] = new DatePipe('en-US').transform(logoutTime, 'dd/MM/yyyy hh:mm:ss');
                })
                this.audits = audits;
            });
    }
    sort(event: any) {
        switch (event.target.value) {
            case 'loginTime':
                this.audits.sort((a, b) => (a.loginTime > b.loginTime ? -1 : 1));
                break;
            case 'ID':
                this.audits.sort((a, b) => (a.id > b.id ? -1 : 1));
                break;
            case 'User':
                this.audits.sort((a, b) => (a.user > b.user ? -1 : 1));
                break;
        }

    }
    date(event: any) {
        if(event.target.value === '12-hour-format'){
            this.format = 'dd/MM/yyyy hh:mm:ss';
        }else{
            this.format = 'dd/MM/yyyy HH:mm:ss';
        }
        this.audits.map(e => {
            let loginTime = new Date(parseInt(e.loginTime));
            let logoutTime = new Date(parseInt(e.logoutTime));
            e["convertedloginTime"] = new DatePipe('en-IN').transform(loginTime, this.format);
            e["convertedlogoutTime"] = new DatePipe('en-IN').transform(logoutTime, this.format);
        })
    }
}
